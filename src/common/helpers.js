export const humanFileSize = (bytes, si = false, dp = 1) => {
	const threshold = si ? 1000 : 1024;

	if (Math.abs(bytes) < threshold)
  {
	  return bytes + ' B';
	}
  
	const units = si 
	  ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] 
	  : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
	let u = -1;
	const r = 10**dp;
  
	do
  {
	  bytes /= threshold;
	  ++u;
	}
  while (Math.round(Math.abs(bytes) * r) / r >= threshold && u < units.length - 1);
  
  
	return bytes.toFixed(dp) + ' ' + units[u];
};

export const S = {
	M: ['m a r t i n', 'Мартин Иванов'],
	D: ['d o b r o m i r', 'Добромир Колев'],
};

const authPages = [
	'login',
	'register',
];

const protectedPages = [
	'my-addons',
	'profile',
	'admin',
];

export const isAuthPage = (link) => {
	return (authPages.includes(link));
}

export const isProtectedPage = (link) => {
	return (protectedPages.includes(link));
}