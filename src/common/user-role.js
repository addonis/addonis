export const userRole = {
  BLOCKED: 0,
  BASIC: 1,
  MODERATOR: 2,
  ADMIN: 3,
  OWNER: 4,
};
