import { get, set, ref, query, equalTo, orderByChild, update, onValue, onChildChanged } from 'firebase/database';
import { db } from '../common/firebase-config';
import { userRole } from '../common/user-role';

export const getUserByHandle = (handle) => {

  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (handle, firstName, lastName, uid, email) => {

  return set(ref(db, `users/${handle}`), { username: handle, firstName, lastName, uid, email, createdOn: new Date(), addons: {}, role: userRole.BASIC });
};

export const getUserData = (uid) => {

  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updateUserData = (username, data) => {

	let result = {};

	Object.keys(data).forEach(key => {
	  result[`users/${username}/${key}`] = data[key];
	});
  
  return update(ref(db), result);
};

export const updateUserProfile = (handle, firstName, lastName, phoneNumber) => {

  return update(ref(db), {
    [`users/${handle}/firstName`]: firstName,
    [`users/${handle}/lastName`]: lastName,
    [`users/${handle}/phoneNumber`]: phoneNumber,
  });
};

export const updateUserRole = (handle, role) => {
  return update(ref(db), {
    [`users/${handle}/role`]: role,
  });
};

export const updateUserProfilePicture = (handle, url) => {
  return update(ref(db), {
    [`users/${handle}/avatarUrl`]: url,
  });
};

export const getUsersCount = () => {
  return get(ref(db, 'users'))
    .then(snapshot => {
      if (!snapshot.exists()) {
        return 0;
      }

      return Object.keys(snapshot.val()).length;
    });
}

export const getLiveUserData = (username, listen) => {
	return onValue(ref(db, `users/${username}`), () => {
	  get(ref(db, `users/${username}`))
	  .then(listen);
	});
};

export const getLiveUsersData = (listen) => {
  return onChildChanged(ref(db, 'users'), () => {
    get(ref(db, 'users'))
    .then(listen);
  });
};

export const getUsersData = () => {
  return get(ref(db, 'users'));
}
