import {
  createUserWithEmailAndPassword, 
  signInWithEmailAndPassword,
  reauthenticateWithCredential,
  updateProfile,
  updatePassword,
  signOut
} from 'firebase/auth';
import { auth } from '../common/firebase-config';

export const registerUser = (email, password) => {
  return createUserWithEmailAndPassword(auth, email, password);
};

export const loginUser = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password);
};

export const updateUser = (data) => {
	return updateProfile(auth.currentUser, data);
};

export const changePassword = (email, passwordOld, passwordNew) => {
  loginUser(email, passwordOld).then(u => {
    reauthenticateWithCredential(auth.currentUser, u).then(() => {
      return updatePassword(auth.currentUser, passwordNew);
    }).catch((error) => {
    });
  });
};

export const logoutUser = () => {
  return signOut(auth);
};
