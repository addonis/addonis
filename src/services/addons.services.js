import { get, set, ref, query, equalTo, orderByChild, update, onChildChanged, remove } from 'firebase/database';
import { db } from '../common/firebase-config';
import moment from 'moment';

export const createAddon = (id, data) => {

  return set(ref(db, `addons/${id}`), data);
};

export const getAddon = (id) => {

	return get(ref(db, `addons/${id}`));
};

export const getAddons = () => {
  return get(ref(db, 'addons'));
}

export const getApprovedAddons = () => {

  return get(query(ref(db, 'addons'), orderByChild('isApproved'), equalTo(true)));
};

export const getFeaturedAddons = async (numberAddons) => {

  let result = [];

  await getApprovedAddons().then(snapshot => {
    if (snapshot.exists())
    {
      const records = snapshot.val();

      if (!records)
      {
        return result;
      }

      Object.keys(records).map(key => {
        if (records[key]['isFeatured'] === true)
        {
          result.push({ ...records[key], id: key });
        }

		    return records[key];
      });

      if (typeof(numberAddons) === 'number' && numberAddons > 0)
      {
        result = result.slice(0, numberAddons);
      }
    }
  }).catch();

  return result;
};

export const getMostDownloadedAddons = async (numberAddons) => {

	let result = [];

	await getApprovedAddons().then(snapshot => {
	  if (snapshot.exists())
	  {
		  const records = snapshot.val();
  
		  if (!records)
		  {
		    return result;
		  }
  
		  Object.keys(records).map(key => result.push({...records[key], id: key}));

      result.sort((a, b) => {
        return a.downloads - b.downloads;
      }).reverse();
    
      if (typeof(numberAddons) === 'number' && numberAddons > 0)
      {
        result = result.slice(0, numberAddons);
      }

      return result;
    }
	}).catch();

	return result;
};

export const getMostRecentAddons = async (numberAddons) => {

	let result = [];

	await getApprovedAddons().then(snapshot => {
	  if (snapshot.exists())
	  {
		  const records = snapshot.val();
  
		  if (!records)
		  {
		    return result;
		  }
  
		  Object.keys(records).map(key => result.push({...records[key], id: key}));

      result.sort((a, b) => {
        return moment(a.uploadDate) - moment(b.uploadDate);
      }).reverse();
    
      if (typeof(numberAddons) === 'number' && numberAddons > 0)
      {
        result = result.slice(0, numberAddons);
      }

      return result;
    }
	}).catch();

	return result;
};

export const getAddonsByUser = (user) => {

  return get(query(ref(db, 'addons'), orderByChild('user'), equalTo(user)));
};

export const updateAddon = (id, data) => {

  let result = {};

  Object.keys(data).forEach(key => {
    result[`addons/${id}/${key}`] = data[key];
  });

  return update(ref(db), result);
};

export const removeAddon = (id) => {

  return remove(ref(db, `addons/${id}`));
};

export const getAddonsCount = () => {
  return get(ref(db, 'addons'))
    .then(snapshot => {
      if (!snapshot.exists()) {
        return 0;
      }

      return Object.keys(snapshot.val()).length;
    });
}

export const getLiveAddonsData = (listen) => {
	return onChildChanged(ref(db, 'addons'), () => {
	  get(ref(db, 'addons'))
	  .then(listen);
	});
};

export const getLiveApprovedAddonsData = (listen) => {
	return onChildChanged(ref(db, 'addons'), () => {
    get(query(ref(db, 'addons'), orderByChild('isApproved'), equalTo(true)))
	  .then(listen);
	});
};

export const getAddonsTags = async (numberEntries = 10) => {
  let result = [];

	await getApprovedAddons().then(snapshot => {
	  if (snapshot.exists())
	  {
		  const records = snapshot.val();
  
		  if (!records)
		  {
		    return result;
		  }
  
		  Object.keys(records).map(key => {
        if (records[key]['tags'])
        {
          records[key]['tags'].map(tag => result.push(tag.toLowerCase()));
        }

        return records[key];
      });

      // Get unique values only
      result = result.filter((value, index, self) => {
        return self.indexOf(value) === index;
      });

      // Sort the values
      result.sort();
    
      // Get only the specified number of values
      result = result.slice(0, numberEntries);

      return result;
    }
	}).catch();

	return result;
};

export const getAddonsIde = async (numberEntries = 10) => {
  let result = [];

	await getApprovedAddons().then(snapshot => {
	  if (snapshot.exists())
	  {
		  const records = snapshot.val();
  
		  if (!records)
		  {
		    return result;
		  }
  
		  Object.keys(records).map(key => result.push(records[key]['ide']?.toLowerCase()));

      // Get unique values only
      result = result.filter((value, index, self) => {
        return self.indexOf(value) === index;
      });

      // Sort the values
      result.sort();
    
      // Get only the specified number of values
      result = result.slice(0, numberEntries);

      return result;
    }
	}).catch();

	return result;
};
  
