import { storage } from "../common/firebase-config";
import { ref, uploadBytes, listAll, getDownloadURL, getMetadata, deleteObject } from "firebase/storage";

export const uploadFile = (id, imageUpload, metaData) => {

  return uploadBytes(ref(storage, `addons/${id}`), imageUpload, metaData);
};

export const getUploadURL = (id) => {

  return getDownloadURL(ref(storage, `addons/${id}`));
};

export const getUploadMetadata = (id) => {

  return getMetadata(ref(storage, `addons/${id}`));
}

export const removeFile = (id) => {

  if (typeof(id) === 'undefined' || !id.length)
  {
    return Promise.resolve();
  }

  return deleteObject(ref(storage, `addons/${id}`));
}

export const getAllUploads = () => {

  return listAll(ref(storage, `addons/`));
};
