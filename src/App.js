import { useState, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './common/firebase-config';
import { getUserData } from './services/users.services';
import { getUploadURL } from "./services/uploads.services";
import AppContext from './providers/AppContext';
import "./App.css";
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { Home } from "./views/Home/Home";
import { Login } from "./views/Login/Login";
import { Register } from "./views/Register/Register";
import { Profile } from "./views/Profile/Profile";
import { About } from "./views/About/About";
import { AddonsListing } from "./views/Addon/AddonsListing";
import { AddonForm } from "./views/Addon/AddonForm";
import { Addons } from "./views/Addon/Addons";
import { Addons as AdminAddons } from "./views/Admin/Addons";
import { Users as AdminUsers } from "./views/Admin/Users";
import Authenticated from "./hoc/Authenticated";

function App() {
   
  const [appState, setAppState] = useState({
    user: null,
    userData: { handle: null },
	  userAvatar: '',
  });
  
  let [user, loading, error] = useAuthState(auth);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then(snapshot => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        const data = snapshot.val()[Object.keys(snapshot.val())[0]];

		    if (typeof(data.avatar) !== 'undefined' && data.avatar.length)
        {
          getUploadURL(data.avatar).then((url) => {
            setAppState({
              user,
              userData: data,
              userAvatar: url,
            });
          });
        }
        else
        {
          setAppState({
            user,
            userData: data,
            userAvatar: '',
          });
        }
	    })
      .catch(e => alert(e.message));
  }, [user]);

  return (
  <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
    <div className="w-full">
      <Header />
      <main className="w-full pb-20">
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/addons" element={<Addons />}></Route>
          <Route path="/addons/featured" element={<Addons type="featured" />}></Route>
          <Route path="/addons/top" element={<Addons type="top" />}></Route>
          <Route path="/about" element={<About />}></Route>

          <Route path="/login" element={<Authenticated loading={ loading }><Login /></Authenticated>}></Route>
          <Route path="/register" element={<Authenticated loading={ loading }><Register /></Authenticated>}></Route>

          <Route path="/my-addons" element={<Authenticated loading={ loading }><AddonsListing /></Authenticated>}></Route>
          <Route path="/my-addons/add" element={<Authenticated loading={ loading }><AddonForm /></Authenticated>}></Route>
          <Route path="/my-addons/edit/:id" element={<Authenticated loading={ loading }><AddonForm /></Authenticated>}></Route>
          <Route path="/profile" element={<Authenticated loading={ loading }><Profile /></Authenticated>}></Route>
          <Route path="/admin" element={<Authenticated loading={ loading }><AdminAddons /></Authenticated>}></Route>
          <Route path="/admin/addons" element={<Authenticated loading={ loading }><AdminAddons /></Authenticated>}></Route>
          <Route path="/admin/users" element={<Authenticated loading={ loading }><AdminUsers /></Authenticated>}></Route>
        </Routes>
      </main>
      <Footer />
    </div>
  </AppContext.Provider>
  );
}

export default App;
