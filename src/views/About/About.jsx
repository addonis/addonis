export const About = () => {
  return (
    <section className="p-8">
      <h2 className="text-2xl mb-12">About Us</h2>
    <div className="display: flex justify-around ">
      
       <div className=" text-center"><img src="https://firebasestorage.googleapis.com/v0/b/addonis-e66a7.appspot.com/o/adons%2F_643459100123993_4514733266690789149_n.jpg?alt=media&token=63e5f3d0-88ec-4139-b081-560de18c3a79" alt="Martin" width={400}/>
     <h2 className="text-3xl mb-1 mt-1 	"> Martin Ivanov</h2>
     <h2> <a href="mailto:martin.ivanov.ivanov@gmail.com" className="text-decoration-line: underline text-lg mb-1 mt-1 text-center">martin.ivanov.ivanov@gmail.com</a></h2>
     <h2 className="text-lg mb-1 mt-1"> <a href="https://gitlab.com/MartinIv" className="text-decoration-line: underline text-center">https://gitlab.com/MartinIv</a></h2>
     
     </div>
     
     <div className="display: inline-block text-center"><img src="https://firebasestorage.googleapis.com/v0/b/addonis-e66a7.appspot.com/o/adons%2F286958176_993051314738524_8794046587437738478_n.jpg?alt=media&token=575a3102-f0d8-4de8-85f4-0902ad4af936?alt=media&token=eb26651f-bc6a-47b7-93c4-8c5f5f2e4db4" alt="Martin" width={400}/>
     <h2 className="text-3xl mb-1 mt-1 text-center	"> Dobromir Kolev</h2>
     <h3> <a href="mailto:dobromirkolev1999@gmail.com" className="text-decoration-line: underline text-lg mb-1 mt-1 text-center">dobromirkolev1999@gmail.com</a></h3>
     <h2 className="text-lg mb-1 mt-1"> <a href="https://gitlab.com/DobromirKolev" className="text-decoration-line: underline text-center ">https://gitlab.com/DobromirKolev</a></h2>
     
     </div>
     </div>
    </section>
  )
}
