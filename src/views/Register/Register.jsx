import { useState } from "react";
import { registerUser } from "../../services/auth.service";
import {
  getUserByHandle,
  createUserHandle,
} from "../../services/users.services";
import { useNavigate, Link } from "react-router-dom";
import { FormInput } from "../../components/FormInput";
import { Button } from "../../components/Button";

export const Register = () => {
  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    username: "",
    email: "",
    password: "",
    passwordConfirm: "",
  });

  const [message, setMessage] = useState({
    firstName: "",
    lastName: "",
    username: "",
    email: "",
    password: "",
    passwordConfirm: "",
  });

  const [status, setStatus] = useState({
    firstName: 'hidden',
    lastName: 'hidden',
    username: 'hidden',
    email: 'hidden',
    password: 'hidden',
    passwordConfirm: 'hidden',
  });

  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const register = (e) => {
    e.preventDefault();

    setMessage({
      firstName: "",
      lastName: "",
      username: "",
      email: "",
      password: "",
      passwordConfirm: "",
    });

    setStatus({
      firstName: 'hidden',
      lastName: 'hidden',
      username: 'hidden',
      email: 'hidden',
      password: 'hidden',
      passwordConfirm: 'hidden',
    });

    if (!form.email.length) {
      setMessage({ email: "Provide a valid email address!" });
      setStatus({ email: "error" });
      return;
    }

    if (form.password.length < 6) {
      setMessage({ password: "Password should be at least 6 characters!" });
      setStatus({ password: "error" });
      return;
    }

    if (form.passwordConfirm !== form.password) {
      setMessage({ passwordConfirm: "Re-typed password is different!" });
      setStatus({ passwordConfirm: "error" });
      return;
    }

    // if (form.firstName.length < 6) {
    //   return alert('Password shoud be 6 or more');
    // }
    getUserByHandle(form.username)
      .then((snapshot) => {
        if (snapshot.exists()) {
          setMessage({
            username: `Profile with username @${form.username} already exists!`,
          });
          setStatus({ username: "error" });
          return;
        }

        return registerUser(form.email, form.password)
          .then((u) => {
            createUserHandle(
              form.username,
              form.firstName,
              form.lastName,
              u.user.uid,
              u.user.email
            )
              .then(() => {
                navigate("/");
              })
              .catch(console.error);
          })
          .catch((e) => {
            if (e.message.includes(`email-already-in-use`)) {
              setMessage({
                email: `Email ${form.email} has already been registered!`,
              });
              setStatus({ email: "error" });
            }
          });
      })
      .catch(console.error);
  };

  return (
    <section className="p-8">
      <h2 className="text-2xl text-center mb-4">Register</h2>
      <form className="w-1/4 mx-auto mt-12">
        <div>
          <FormInput id="firstName" value={ form.firstName } placeholder="First Name" onChange={ updateForm('firstName') } />
          <span className={status.firstName}>{message.firstName}</span>
        </div>
        <div>
          <FormInput id="lastName" value={ form.lastName } placeholder="Last Name" onChange={ updateForm('lastName') } />
          <span className={status.lastName}>{message.lastName}</span>
        </div>
        <div>
          <FormInput id="username" value={ form.username } placeholder="Username" onChange={ updateForm('username') } />
          <span className={status.username}>{message.username}</span>
        </div>
        <div>
          <FormInput id="email" type="email" value={ form.email } placeholder="Email Address" isRequired={ true } onChange={ updateForm('email') } />
          <span className={status.email}>{message.email}</span>
        </div>
        <div>
          <FormInput id="password" type="password" value={ form.password } placeholder="Password" isRequired={ true } onChange={ updateForm('password') } />
          <span className={status.password}>{message.password}</span>
        </div>
        <div>
          <FormInput id="passwordConfirm" type="password" value={ form.passwordConfirm } placeholder="Re-type password" isRequired={ true } onChange={ updateForm('passwordConfirm') } />
          <span className={status.passwordConfirm}>{message.passwordConfirm}</span>
        </div>
        <div className="flex justify-center">
          <Button text="Register" onClick={ register } className="w-48 bg-sky-600 text-white text-xl shadow-xl rounded-md px-4 py-2 hover:bg-orange-400 hover:text-gray-700" />
        </div>
        <div className="text-center mt-2">
          <Link to="/login" className="font-semibold text-blue-900 underline">Already a member?</Link>
        </div>
      </form>
    </section>
  );
};
