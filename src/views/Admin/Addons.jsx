import { useState, useEffect } from 'react';
import moment from 'moment';
import { getAddons, getLiveAddonsData } from '../../services/addons.services';
import { AddonRow } from './AddonRow';

export const Addons = () => {
  const [addonsData, setAddonsData] = useState([]);
  const [searchField, setSearchField] = useState("");

  useEffect(() => {
    const listen = (snapshot) => {
      if (!snapshot.exists())
      {
        setAddonsData([]);
      }

      const data = Object.keys(snapshot.val()).map(key => { return { ...snapshot.val()[key], id: key } });

      data.sort((a, b) => {
        return moment(a.uploadDate) - moment(b.uploadDate);
      }).reverse();

      setAddonsData(data);
    }

    getAddons().then(listen);

    const unsubscribe = getLiveAddonsData(listen);

    return () => unsubscribe();
  }, []);

  const filteredAddons = addonsData.filter(addon => {
    return (
      addon.name?.toLowerCase().includes(searchField.toLowerCase()) ||
      addon.description?.toLowerCase().includes(searchField.toLowerCase()) ||
      addon.version?.toLowerCase().includes(searchField.toLowerCase()) ||
      addon.ide?.toLowerCase().includes(searchField.toLowerCase()) ||
      addon.user?.toLowerCase().includes(searchField.toLowerCase())
    );
  });

  const handleChange = (e) => {
    //console.log(searchField);
    //console.log(filteredAddons);
    setSearchField(e.target.value);
  };

  return (
    <section className="p-8">
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-2xl">Manage Addons</h1>
        <input type="search" placeholder="Search addons" onChange={ handleChange } className="border border-solid border-gray-200 px-4 py-2 outline-none" />
      </div>
      <div>
        <table className="w-full border-collapse">
          <thead>
            <tr>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">#</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Image</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Name</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Description</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Version</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Target IDE</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Tags</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">User</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Upload Date</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Status</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Featured</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Actions</th>
            </tr>
          </thead>
          <tbody>
            { filteredAddons.map((addon, index) => <AddonRow key={ index } index={ (index + 1)} addon={ addon } />) }
          </tbody>
        </table>
      </div>
    </section>
  )
}
