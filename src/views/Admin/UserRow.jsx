import { useContext, useState, useEffect } from 'react';
import { FaBan, FaCheck, FaUserCog } from 'react-icons/fa';
import AppContext from '../../providers/AppContext';
import { getUploadURL } from '../../services/uploads.services';
import { userRole } from '../../common/user-role';
import { Button } from '../../components/Button';

export const UserRow = ({index, currentUserData, blockUser, unblockUser, makeAdmin}) => {
  const { userData: { username, role} } = useContext(AppContext);

  const [avatarImage, setAvatarImage] = useState('');

  useEffect(() => {
    if (typeof(currentUserData.avatar) !== 'undefined' && currentUserData.avatar.length)
    {
      getUploadURL(currentUserData.avatar).then((url) => {
        setAvatarImage(url);
      });
    }

  }, []);

  const isUserBlocked = () => {
    return currentUserData.role === userRole.BLOCKED;
  }

  return (
		<tr className={ (currentUserData.username === username) ? 'bg-orange-50' : '' }>
		  <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ index }</td>
		  <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ avatarImage && <img src={ avatarImage } alt="" className="inline-block max-w-none h-20" /> }</td>
		  <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ currentUserData.firstName } { currentUserData.lastName }</td>
		  <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ currentUserData.phoneNumber }</td>
		  <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ currentUserData.email }</td>
		  <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ currentUserData.username }</td>
      <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ Object.keys(userRole)[currentUserData.role] }</td>
		  <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">
		    <span className={ !isUserBlocked() ? 'inline-block bg-green-700 rounded-md text-sm text-white px-2 py-1' : 'inline-block bg-red-700 rounded-md text-sm text-white px-2 py-1' }>{ !isUserBlocked() ? 'Active' : 'Blocked' }</span>
		  </td>
		  <td className="border border-solid border-gray-200 text-xl text-right px-3 py-1">
		    {(currentUserData.role < userRole.ADMIN || role >= userRole.ADMIN) && <Button text={isUserBlocked() ? <FaCheck className="h-8" /> : <FaBan className="h-8" />} onClick={isUserBlocked() ? () => unblockUser(currentUserData.username) : () => {
            if(role < currentUserData.role){
                alert('This user has a higher role, cannot be blocked');
                return;
            }
            blockUser(currentUserData.username);
        }} className={ isUserBlocked() ? 'bg-gradient-to-b from-green-600 to-green-900 border-none text-white px-4 py-2 cursor-pointer' : 'bg-gradient-to-b from-red-600 to-red-900 border-none text-white px-4 py-2 cursor-pointer' } />}
        {(currentUserData.role < userRole.ADMIN && role === userRole.OWNER) && <Button text={ <FaUserCog className="h-8" /> } onClick={() => makeAdmin(currentUserData.username)} className="bg-gradient-to-b from-sky-400 to-sky-700 border-none text-white ml-2 px-4 py-2 cursor-pointer" />}
		  </td>
	  </tr>
  )
};
