import { useState, useEffect } from 'react';
import Moment from "react-moment";
import { FaCheck, FaTimes, FaStar, FaRegStar, FaSearch } from 'react-icons/fa';
import { updateAddon } from "../../services/addons.services";
import { getUploadURL } from "../../services/uploads.services";
import { Button } from "../../components/Button";
import { Popups } from '../../components/Popups';
import { AddonDetails } from './AddonDetails';

export const AddonRow = ({ index, addon }) => {
  
  const [logoImage, setLogoImage] = useState('');
  const [buttonPopup, setButtonPopup] = useState(false);

  useEffect(() => {
    if (typeof(addon.logo) !== 'undefined' && addon.logo.length)
    {
      getUploadURL(addon.logo).then((url) => {
        setLogoImage(url);
      });
    }
  }, []);

  const isApproved = () => {
    return addon.isApproved === true;
  }

  const isFeatured = () => {
    return addon.isFeatured === true;
  }

  const changeApproveState = (id, state) => {
    updateAddon(id, { isApproved: state });
  }

  const changeFeaturedState = (id, state) => {
    updateAddon(id, { isFeatured: state });
  }

  return (
  <tr>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ index }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ logoImage && <img src={ logoImage } alt="" className="inline-block max-w-none h-20" /> }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.name }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ (addon.description.length > 300 ? addon.description.substring(0, 300) + '...' : addon.description) }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.version }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.ide }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.tags && addon.tags.map((tag, index) => (<span key={ index } className="block bg-gray-800 rounded-xl text-xs text-white mb-1 px-2 py-1">{ tag }</span>)) }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.user }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1"><Moment format="DD.MM.YYYY HH:mm">{ addon.uploadDate }</Moment></td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">
      <span className={ addon.isApproved === true ? 'inline-block bg-green-700 rounded-md text-sm text-white px-2 py-1' : 'inline-block bg-gray-500 rounded-md text-sm text-white px-2 py-1' }>{ addon.isApproved === true ? 'Approved' : 'Pending' }</span>
    </td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">
      <span className={ addon.isFeatured === true ? 'inline-block bg-green-700 rounded-md text-sm text-white px-2 py-1' : 'inline-block bg-gray-500 rounded-md text-sm text-white px-2 py-1' }>{ addon.isFeatured === true ? 'Featured' : 'Default' }</span>
    </td>
    <td className="border border-solid border-gray-200 whitespace-nowrap text-xl text-right px-3 py-1">
      { isApproved() ? <Button text={ <FaTimes className="h-8" /> } onClick={() => changeApproveState(addon.id, false)} className="bg-gradient-to-b from-red-600 to-red-900 border-none text-white mr-2 px-4 py-2 cursor-pointer" /> : <Button text={ <FaCheck className="h-8" /> } onClick={() => changeApproveState(addon.id, true)} className="bg-gradient-to-b from-green-600 to-green-900 border-none text-white mr-2 px-4 py-2 cursor-pointer" /> }
      { isFeatured() ? <Button text={ <FaRegStar className="h-8" /> } onClick={() => changeFeaturedState(addon.id, false)} className="bg-gradient-to-b from-gray-200 to-gray-400 border-none text-white mr-2 px-4 py-2 cursor-pointer" /> : <Button text={ <FaStar className="h-8" /> } onClick={() => changeFeaturedState(addon.id, true)} className="bg-gradient-to-b from-green-600 to-green-900 border-none text-white mr-2 px-4 py-2 cursor-pointer" /> }

      <Button text={ <FaSearch className="h-8" /> } onClick={() => setButtonPopup(true)} />
    
      <Popups trigger={buttonPopup} setTrigger={setButtonPopup} className="max-w-7xl text-left whitespace-normal" buttonClassName="text-black top-5 right-10 bg-white px-2 py-1"><AddonDetails addon={ addon } onClick={() => setButtonPopup(false)} /></Popups>
    </td>
  </tr>
  )
}
