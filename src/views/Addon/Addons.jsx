import { useState, useEffect } from 'react';
import { FaStar, FaRegStar } from "react-icons/fa";
import moment from 'moment';
import { getApprovedAddons, getFeaturedAddons, getMostDownloadedAddons, getAddonsIde, getAddonsTags, getLiveApprovedAddonsData } from '../../services/addons.services';
import { Addon } from '../../components/Addon';
import { FormInput } from '../../components/FormInput';

export const Addons = ({ type }) => {
  const [addonsData, setAddonsData] = useState([]);
  const [tagsData, setTagsData] = useState([]);
  const [ideData, setIdeData] = useState([]);
  const [filterTag, setFilterTag] = useState('');
  const [filterIde, setFilterIde] = useState('');
  const [filterStars, setFilterStars] = useState(0);
  const [searchAddon, setSearchAddon] = useState('');

  const starsData = [1, 2, 3, 4, 5];

  useEffect(() => {
    getAddonsTags().then((tags) => {
      setTagsData(tags);
    });

    getAddonsIde().then((ide) => {
      setIdeData(ide);
    });

    const listen = (snapshot) => {
      setAddonsData([]);

      let records;

      if (typeof(snapshot.val) === 'function')
      {
        records = snapshot.val();
      }
      else
      {
        records = snapshot;
      }

      const addons = Object.keys(records).map(key => {
        return { ...records[key], id: key };
      });

      if (!['featured', 'top'].includes(type))
      {
        addons.sort((a, b) => {
          return moment(a.uploadDate) - moment(b.uploadDate);
        }).reverse();
      }

      setAddonsData(addons);
    };

    switch (type)
    {
      case 'featured': // Featured addons
        getFeaturedAddons().then(listen);
        
        break;
      case 'top': // Most downloaded addons
        getMostDownloadedAddons().then(listen);

        break;
      default: // All others
        getApprovedAddons().then(listen);
    }

    const unsubscribe = getLiveApprovedAddonsData(listen);

    return () => unsubscribe();
  }, [type]);

/*  const filteredAddons = addonsData.filter(addon => {
    return ((!filterTag.length && !filterIde.length) ||
      (addon.tags && addon.tags.length && addon.tags.filter(value => value.toLowerCase() === filterTag.toLowerCase()).length)) ||
      (addon.ide && addon.ide.toLowerCase() === filterIde.toLowerCase())
  });*/
  const filteredAddons = addonsData.filter(addon => {
    return (!searchAddon.length ||
      (addon.name && addon.name?.toLowerCase().includes(searchAddon.toLowerCase())));
  }).filter(addon => {
    return (!filterTag.length ||
      (addon.tags && addon.tags.length && addon.tags.filter(value => value.toLowerCase() === filterTag.toLowerCase()).length));
  }).filter(addon => {
    return (!filterIde.length ||
      (addon.ide && addon.ide.toLowerCase() === filterIde.toLowerCase()));
  }).filter(addon => {
    return (!filterStars ||
      (addon.ratings && Math.round(addon.ratings) === filterStars));
  });

  return (
    <section className="p-8">
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-2xl">{ (type === 'featured' ? 'Featured ' : (type === 'top' ? 'Most Downloaded ' : ''))}Addons</h1>
      </div>
      <div className="flex justify-between items-center bg-gray-100 border border-gray-200 mt-6 mb-10 px-4">
        <h2 className="font-semibold text-2xl">Filter by</h2>
        <div className="my-3">
          <h3 className="font-semibold text-md mb-2">Search:</h3>
          <FormInput id="search" type="search" placeholder="Search by addon name" onChange={(e) => setSearchAddon(e.target.value)} className="w-full border border-solid border-gray-200 focus-visible:shadow-lg outline-none px-4 py-2" />
        </div>
        <div className="my-3">
          <h3 className="font-semibold text-md mb-2">Tag:</h3> { tagsData.map((tag, index) => (<span key={ index } onClick={() => setFilterTag((filterTag !== tag ? tag : ''))} className={ filterTag === tag ? "inline-block bg-gradient-to-b from-amber-400 to-orange-500 rounded-xl font-semibold text-sm text-black mb-1 mr-1 px-4 py-2 cursor-pointer" : "inline-block bg-gray-800 rounded-xl text-sm text-white mb-1 mr-1 px-4 py-2 cursor-pointer" }>{ tag }</span>)) }
        </div>
        <div className="my-3">
          <h3 className="font-semibold text-md mb-2">IDE:</h3> { ideData.map((ide, index) => (<span key={ index } onClick={() => setFilterIde((filterIde !== ide ? ide : ''))} className={ filterIde === ide ? "inline-block bg-gradient-to-b from-amber-400 to-orange-500 rounded-xl font-semibold text-sm text-black mb-1 mr-1 px-4 py-2 cursor-pointer" : "inline-block bg-blue-600 rounded-xl text-sm text-white mb-1 mr-1 px-4 py-2 cursor-pointer" }>{ ide }</span>)) }
        </div>
        <div className="my-3">
          <h3 className="font-semibold text-md mb-2">Rating:</h3> { starsData.map((stars, index) => stars <= filterStars ? (<FaStar key={ index } onClick={() => setFilterStars((filterStars !== stars ? stars : ''))} className="inline-block text-4xl text-amber-400 ml-1 cursor-pointer" />) : (<FaRegStar key={ index } onClick={() => setFilterStars((filterStars !== stars ? stars : 0))} className="inline-block text-4xl text-gray-500 ml-1 cursor-pointer" />)) }
        </div>
      </div>
      <div>
        <div className="grid grid-cols-3 gap-6">
          { filteredAddons.length ? filteredAddons.map(data => <Addon key={ data.id } addonData={ data } />) : <h3 className="col-span-3 font-bold text-2xl text-center">No addons found.</h3> }
        </div>
      </div>
    </section>
  )
}
