import { useState, useEffect, useContext } from 'react';
import { useNavigate } from "react-router-dom";
import moment from 'moment';
import AppContext from '../../providers/AppContext';
import { getAddonsByUser } from '../../services/addons.services';
import { Button } from '../../components/Button';
import { AddonRow } from './AddonRow';

export const AddonsListing = () => {
  const { userData } = useContext(AppContext);
  const username = userData.username;

  const navigate = useNavigate();

  const [addonsData, setAddonsData] = useState([]);

  useEffect(() => {
    if (typeof(username) === 'undefined')
    {
      return;
    }

    getAddonsByUser(username).then(snapshot => {
      if (snapshot.exists())
      {
        const records = snapshot.val();

        let addons = [];

        Object.keys(records).map(key => addons.push({ ...records[key], id: key }));

        addons.sort((a, b) => {
          return moment(a.uploadDate) - moment(b.uploadDate);
        }).reverse();

        setAddonsData(addons);
      }
    }).catch(e => console.log(e));
  }, [username, addonsData]);

  return (
    <section className="p-8">
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-2xl">My Addons</h1>
        <Button text="Add New" onClick={() => navigate('/my-addons/add')} />
      </div>
      <div>
        <table className="w-full border-collapse">
          <thead>
            <tr>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">#</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Image</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Name</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Upload Date</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Status</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Downloads</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Rating</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Actions</th>
            </tr>
          </thead>
          <tbody>
            { addonsData.map((addon, index) => <AddonRow key={ index } index={ (index + 1)} addon={ addon } />) }
          </tbody>
        </table>
      </div>
    </section>
  )
}
