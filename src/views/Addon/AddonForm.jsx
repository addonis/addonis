import { useContext, useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { v4 } from "uuid";
import moment from "moment";
import AppContext from '../../providers/AppContext';
import { createAddon, updateAddon, getAddon } from "../../services/addons.services";
import { uploadFile, getUploadURL, getUploadMetadata, removeFile } from "../../services/uploads.services";
import { Button } from "../../components/Button";
import { FormInput } from "../../components/FormInput";
import { FormText } from "../../components/FormText";
import { Upload } from "../../components/Upload";
import { Files } from "../../components/Files";

export const AddonForm = () => {
  const { id } = useParams();

  const navigate = useNavigate();

  const { user, userData } = useContext(AppContext);
  const username = userData.username;

	const [form, setForm] = useState({
		name: '',
		description: '',
		user: '',
		ide: '',
		tags: [],
		//uploadDate: null,
		version: '',
		files: '',
		logo: '',
		//downloads: 0,
		//ratings: 0,
		//isApproved: false,
		//isFeatured: false,
		repo: {
      url: '',
      pr: 0,
      issues: 0,
      lastCommit: '',
    },
  });

  const [logoUpload, setLogoUpload] = useState(null);
  const [logoImage, setLogoImage] = useState('');
  const [filesUpload, setFilesUpload] = useState([]);
  const [addonFiles, setAddonFiles] = useState({ file: '', size: 0, metadata: {} });

  const [validation, setValidation] = useState({
    messages : {
      name: '',
      description: '',
      user: '',
      ide: '',
      tags: '',
      version: '',
      files: '',
      logo: '',
      repo: {
        url: '',
        pr: '',
        issues: '',
        lastCommit: '',
      },
    },
    states: {
      name: 'hidden',
      description: 'hidden',
      user: 'hidden',
      ide: 'hidden',
      tags: 'hidden',
      version: 'hidden',
      files: 'hidden',
      logo: 'hidden',
      repo: {
        url: 'hidden',
        pr: 'hidden',
        issues: 'hidden',
        lastCommit: 'hidden',
      },
    }
	});

  useEffect(() => {
    if (typeof(id) === 'undefined')
    {
      return;
    }

    getAddon(id)
	  .then(snapshot => {
	    if (snapshot.exists())
	    {
        const data = snapshot.val();

        setForm({
          name: data.name,
          description: data.description,
          user: data.user,
          ide: data.ide,
          tags: data.tags ?? [],
          uploadDate: data.uploadDate,
          version: data.version,
          files: data.files ?? '',
          logo: data.logo ?? '',
          downloads: data.downloads,
          ratings: data.ratings,
          isApproved: false,
          isFeatured: false,
          repo: data.repo,
        });

        if (typeof(data.logo) !== 'undefined' && data.logo.length)
        {
          getUploadURL(data.logo).then((url) => {
            setLogoImage(url);
          });
        }

        if (typeof(data.files) !== 'undefined' && data.files.length)
        {
          getUploadURL(data.files).then((url) => {
            setAddonFiles((prev) => { return { ...prev, file: url }; });
          });

          getUploadMetadata(data.files).then((metadata) => {
            setAddonFiles((prev) => { return { ...prev, size: metadata.size, metadata: metadata }; });
          });
        }

        /*if (typeof(data.files) !== 'undefined' && data.files.length)
        {
          data.files.map((file) => {
            getUploadURL(file).then((url) => {
              setAddonFiles([ ...addonFiles, url ]);
            });
          })
        }*/
	    }
    })
    .catch(console.error);
  }, [, logoImage, filesUpload]);

  const updateForm = prop => e => {
		setForm({
		  ...form,
		  [prop]: (prop === 'tags' ? e.target.value.split(', ') : e.target.value),
		});
	};

  const changeLogoFile = (e) => {
    setLogoUpload(e.target.files[0]);
  };

  const changeAddonFiles = (e) => {
    console.log('changeAddonFiles');
    //setFilesUpload([ ...filesUpload, e.target.files[0]]);
    setFilesUpload(e.target.files[0]);
    console.log(filesUpload);
  };

  const addAddon = (e) => {
		e.preventDefault();

    const id = v4();
    const currentDate = new Date();

		setValidation({
      messages: {
        name: '',
        description: '',
        user: '',
        ide: '',
        tags: '',
        version: '',
        files: '',
        logo: '',
        repo: {
          url: '',
          pr: '',
          issues: '',
          lastCommit: '',
        },
      },
      states: {
        name: 'hidden',
        description: 'hidden',
        user: 'hidden',
        ide: 'hidden',
        tags: 'hidden',
        version: 'hidden',
        files: 'hidden',
        logo: 'hidden',
        repo: {
          url: 'hidden',
          pr: 'hidden',
          issues: 'hidden',
          lastCommit: 'hidden',
        },
      }
		});
	
    if (logoUpload !== null)
    {
      uploadFile(id, logoUpload).then((file) => {
  
        setForm({ ...form, logo: id });

        getUploadURL(id).then((url) => {
          setLogoImage(url);
        });
      })
    }

    let key = id;

    if (filesUpload !== null && filesUpload.name?.length)
    {
      const nameParts = filesUpload.name.split('.');
      const extension = nameParts[(nameParts.length - 1)];

      key = `${id}_1.${extension}`;

      uploadFile(key, filesUpload, { customMetadata: { extension: extension } }).then((file) => {
        setForm({ ...form, files: key });

        getUploadURL(key).then((url) => {
          setAddonFiles((prev) => { return { ...prev, file: url, size: file.metadata.size, metadata: file.metadata }; });
        });
      })
    }

    const data = {
      ...form,
      user: userData.username,
      uploadDate: moment().format(),
      logo: (logoUpload !== null ? id : form.logo),
      files: (filesUpload !== null  ? key : form.files),
      downloads: 0,
		  ratings: 0,
		  isApproved: false,
		  isFeatured: false,
    }

    createAddon(id, data)
		  .then(() => {
        navigate('/my-addons');

        return;
	  });
  };

  const editAddon = (e) => {
		e.preventDefault();

    const currentDate = new Date();

		setValidation({
      messages: {
        name: '',
        description: '',
        user: '',
        ide: '',
        tags: '',
        version: '',
        files: '',
        logo: '',
        repo: {
          url: '',
          pr: '',
          issues: '',
          lastCommit: '',
        },
      },
      states: {
        name: 'hidden',
        description: 'hidden',
        user: 'hidden',
        ide: 'hidden',
        tags: 'hidden',
        version: 'hidden',
        files: 'hidden',
        logo: 'hidden',
        repo: {
          url: 'hidden',
          pr: 'hidden',
          issues: 'hidden',
          lastCommit: 'hidden',
        },
      }
		});

    if (logoUpload !== null)
    {
      uploadFile(id, logoUpload).then((file) => {
        setForm({ ...form, logo: id });

        getUploadURL(id).then((url) => {
          setLogoImage(url);
        });
      })
    }

    let key = id;

    if (filesUpload !== null && filesUpload.name?.length)
    {
      const nameParts = filesUpload.name.split('.');
      const extension = nameParts[(nameParts.length - 1)];

      key = `${id}_1.${extension}`;

      uploadFile(key, filesUpload, { customMetadata: { extension: extension } }).then((file) => {
        removeFile(form.files).then(() => {
          setForm((prev) => { return { ...prev, files: '' }; });
          
          getUploadURL(key).then((url) => {
            setAddonFiles((prev) => { return { ...prev, file: url, size: file.metadata.size, metadata: file.metadata }; });
          });
        });
      })
    }

    /*if (filesUpload.length)
    {
      filesUpload.map((record, index) => {
        const nameParts = record.name.split('.');

        const key = `${id}_${(index + 1)}.${ nameParts[(nameParts.length - 1)]}`;

        uploadFile(key, record).then((file) => {
          console.log(file);
          getUploadURL(key).then((url) => {
            setForm({ ...form, files: [ ...form.files, url ] });

            setAddonFiles([ ...addonFiles, url ]);
          });
        })
      })
    }*/

    const data = {
      ...form,
      logo: (logoUpload !== null ? id : form.logo),
      //files: (filesUpload.length ? addonFiles : form.files),
      files: (filesUpload !== null  ? key : form.files),
      uploadDate: moment().format(),
      isApproved: false,
      isFeatured: false,
    }

    updateAddon(id, data)
      .then(() => {
        //navigate('/my-addons');
    });

  };

  return (
    <section className="p-8">
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-2xl">{ typeof(id) !== 'undefined' ? `Edit addon ${form.name}` : 'Add New Addon' }</h1>
        <Button text="Back" onClick={() => { navigate('/my-addons') }} />
      </div>
	    <div>
		    <form className='w-full mx-auto mt-12'>
          <div className="grid grid-cols-3 gap-6">
            <div>
              <label htmlFor="logo" className="block font-medium mb-2">Addon Image</label>
              { logoImage && <img src={ logoImage } alt="" className="mb-4" /> }
              <Upload id="logo" text="Upload Image" label="Upload Image" accept="image/png,image/jpeg,image/gif" onChange={(e) => changeLogoFile(e)} />
            </div>
            <div>
              <div>
                <FormInput id="name" value={ form.name } placeholder="Addon Name" label="Addon Name" onChange={ updateForm('name') } />
                <span className={ validation.states.name }>{ validation.messages.name }</span>
              </div>
              <div className='user-box'>
                <FormText id="description" value={ form.description } placeholder="Addon Description" label="Addon Description" onChange={ updateForm('description') } />
                <span className={ validation.states.description }>{ validation.messages.description }</span>
              </div>
              <div>
                <FormInput id="ide" value={ form.ide } placeholder="Target IDE (ex. VS, VSC)" label="Target IDE (ex. VS, VSC)" onChange={ updateForm('ide') } />
                <span className={ validation.states.ide }>{ validation.messages.ide }</span>
              </div>
              <div>
                <FormInput id="tags" value={ form.tags.join(', ') } placeholder="Tags separated with comma" label="Tags separated with comma" onChange={ updateForm('tags') } />
                <span className={ validation.states.ide }>{ validation.messages.ide }</span>
              </div>
              <div>
                <FormInput id="version" value={ form.version } placeholder="Addon Version" label="Addon Version" onChange={ updateForm('version') } />
                <span className={ validation.states.version }>{ validation.messages.version }</span>
              </div>
            </div>
            <div>
              <label htmlFor="files" className="block font-medium mb-2">Addon Files</label>
              { addonFiles && <Files files={ addonFiles } /> }
              {/*Files to be uploaded:
              { filesUpload && <ul>{ filesUpload.map((file, index) => (<li key={ index }>{ `${file.name} - ${file.size}` }</li>))}</ul> }*/}
              <Upload id="files" text="Upload Addon File" label="Upload File" onChange={(e) => changeAddonFiles(e)} />

            </div>
          </div>
          <div className="flex justify-center">
            { typeof(id) !== 'undefined' ? (<Button text='Update' onClick={ editAddon } className="w-48 bg-sky-600 text-white text-xl shadow-xl rounded-md px-4 py-2 hover:bg-orange-400 hover:text-gray-700" />) : (<Button text='Add' onClick={ addAddon } className="w-48 bg-sky-600 text-white text-xl shadow-xl rounded-md px-4 py-2 hover:bg-orange-400 hover:text-gray-700" />)}
          </div>
		    </form>
	    </div>
    </section>
  )
}
