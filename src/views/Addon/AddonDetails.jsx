import { useState, useEffect } from "react";
import Moment from "react-moment";
import FileSaver from "file-saver";
import { humanFileSize } from '../../common/helpers';
import { getUploadURL, getUploadMetadata } from '../../services/uploads.services';
import { updateAddon } from '../../services/addons.services';
import { Button } from "../../components/Button";
import { Ratings } from "../../components/Ratings";

export const AddonDetails = ({ addon }) => {
	const [logoImage, setLogoImage] = useState('');
	const [downloadFile, setDownloadFile] = useState({ file: '', size: 0, metadata: {} });
/*
  const addon = {
    id: id,
    name: "Test Addon 2",
    description: "Full description of test addon",
    user: "bla123",
    ide: "VSC",
    tags: ["tag1", "tag2", "tag3"],
    uploadDate: currentDate.toLocaleString(),
    version: '1.0',
    files: { [`${id}`]: '1.0' },
    logo: id + '_logo',
    downloads: 0,
    ratings: 0,
    isApproved: false,
    isFeatured: false,
    repo: { url: "", pr: 0, issues: 0, lastCommit: "" },
  };
*/

  useEffect(() => {
    if (typeof(addon.logo) !== 'undefined' && addon.logo.length)
    {
      getUploadURL(addon.logo).then((url) => {
        setLogoImage(url);
      });
    }

    if (typeof(addon.files) !== 'undefined' && addon.files.length)
    {
      getUploadURL(addon.files).then((url) => {
        setDownloadFile((prev) => {
          return { ...prev, file: url };
        });
      });

      getUploadMetadata(addon.files).then((metadata) => {
        setDownloadFile((prev) => {
          return { ...prev, size: metadata.size, metadata: metadata };
        });
      });
    }
  }, []);

  const handleDownload = (file) => {
    updateAddon(addon.id, { downloads: (addon.downloads + 1) }).then(() => {
      FileSaver.saveAs(file);
    });
  }

  return (
    <section className="max-h-screen overflow-y-auto bg-white my-6 py-5">
      <div className="flex justify-between items-start px-6 py-4">
        <div>
          <div className="flex flex-nowrap items-start gap-4 mb-2">
            <h1 className="font-semibold text-2xl">{ addon.name }</h1>
            <span className="bg-blue-600 rounded-md text-base text-white text-center px-2 py-1">{ addon.ide }</span>
          </div>
          <div>
            <Ratings addon={ addon } hideValue={ true } disableClick={ true } />
          </div>
          <div className="text-sm text-gray-500 mt-2">{ addon.user }</div>
        </div>
        <div className="text-right mt-2">
          { downloadFile.file && <Button text={ `Download (${ humanFileSize(downloadFile.size, true, 2) })` } onClick={() => handleDownload(downloadFile.file)} /> }
          <div className="text-xs text-gray-100 hidden">{ addon.id }</div>
          <div className="text-xs text-gray-500 mt-2">version { addon.version } (<Moment format="DD.MM.YYYY">{ addon.uploadDate }</Moment>)</div>
          <div className="text-xs text-gray-500 mt-1">{ addon.downloads } download{ addon.downloads !== 1 && 's' }</div>
        </div>
      </div>
      <div className="border-t border-gray-200 my-2"></div>
      <div className="flex-1 text-base px-6 py-4">
        { logoImage && <img src={ logoImage } alt={ addon.name } className="max-w-xl float-left mb-4 mr-4" /> }
        { addon.description }
        <div className="clear-both"></div>
      </div>
      <div className="border-t border-gray-200 my-2"></div>
      <div className="flex-1 text-base px-6 py-4">
        <div className="flex items-end w-full">{ addon.tags && addon.tags.map((tag, index) => (<span key={ index } className="inline-block bg-gray-800 rounded-xl text-sm text-white mb-1 mr-1 px-2 py-1">{ tag }</span>)) }</div>
      </div>
    </section>
  )
}

