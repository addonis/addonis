import { useParams, useNavigate } from "react-router-dom";

export const AddonEdit = () => {
  const { id } = useParams();

  const navigate = useNavigate();

  return (
    <section>
      <h1>Edit addon #{ id }</h1>
      <button type="button" onClick={() => { navigate('/my-addons') }} className="btn">Back</button>
    </section>
	
  )
}
