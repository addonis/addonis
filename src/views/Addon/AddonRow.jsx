import { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import Moment from "react-moment";
import { FaPencilAlt, FaTrashAlt } from 'react-icons/fa';
import { removeAddon } from "../../services/addons.services";
import { getUploadURL } from "../../services/uploads.services";
import { Button } from "../../components/Button";
import { Popups } from '../../components/Popups';

export const AddonRow = ({ index, addon }) => {
  const navigate = useNavigate();
  
  const [logoImage, setLogoImage] = useState('');
  const [buttonPopup, setButtonPopup] = useState(false);

  useEffect(() => {
    if (addon.logo.length)
    {
      getUploadURL(addon.logo).then((url) => {
        setLogoImage(url);
      });
    }
  }, []);

  const deleteRecord = (id) => {
    removeAddon(id).then(snapshot => {
      navigate('/my-addons');
    }).catch();
  };

  return (
	<tr>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ index }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ logoImage && <img src={ logoImage } alt="" className="inline-block max-w-none h-20" /> }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.name }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1"><Moment format="DD.MM.YYYY HH:mm">{ addon.uploadDate }</Moment></td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">
      <span className={ addon.isApproved === true ? 'inline-block bg-green-700 rounded-md text-sm text-white px-2 py-1' : 'inline-block bg-gray-500 rounded-md text-sm text-white px-2 py-1' }>{ addon.isApproved === true ? 'Approved' : 'Waiting approval' }</span>
    </td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.downloads }</td>
    <td className="border border-solid border-gray-200 text-xl text-center px-3 py-1">{ addon.ratings.toFixed(2) }</td>
    <td className="border border-solid border-gray-200 text-xl text-right px-3 py-1">
      <Button text={ <FaPencilAlt className="h-8" /> } onClick={() => { navigate('/my-addons/edit/' + addon.id); }} className="bg-gradient-to-b from-sky-400 to-sky-700 border-none text-white px-4 py-2 cursor-pointer" />
      <Button text={ <FaTrashAlt className="h-8" /> } onClick={() => setButtonPopup(true)} className="bg-gradient-to-b from-red-600 to-red-900 border-none text-white ml-2 px-4 py-2 cursor-pointer" />
      <Popups trigger={buttonPopup} setTrigger={setButtonPopup}>
        <div className="bg-white rounded-lg shadow-md text-left z-10">
          <div className="bg-red-800 rounded-t-lg px-4 py-3">
            <h3 className="text-lg text-white text-shadow text-shadow-sm">Remove Addon</h3>
          </div>
          <div className="text-xl px-4 py-8">Are you sure you want to delete addon #{index} ?</div>
          <div className="flex justify-end border-t border-solid border-gray-200 text-right p-2">
            <Button text="Yes" onClick={() => deleteRecord(addon.id)} className="bg-gradient-to-b from-red-600 to-red-900 border-none text-white px-4 py-2 cursor-pointer" />
            <Button text="No" onClick={() => setButtonPopup(false)} className="bg-gradient-to-b from-gray-200 to-gray-400 border-none ml-2 px-4 py-2 cursor-pointer" />
          </div>
        </div>
    </Popups>
    </td>
  </tr>
  )
}
