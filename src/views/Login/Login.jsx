import { useState, useContext } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import AppContext from '../../providers/AppContext';
import { loginUser } from '../../services/auth.service';
import { getUserData } from '../../services/users.services';
import { Button } from '../../components/Button';
import { FormInput } from '../../components/FormInput';

export const Login = () => {
  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const { setContext } = useContext(AppContext);
  const navigate = useNavigate();

  const updateForm = prop => e => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const login = (e) => {
    e.preventDefault();

    loginUser(form.email, form.password)
      .then(u => {
        return getUserData(u.user.uid)
          .then(snapshot => {
            if (snapshot.exists()) {
              setContext({
                user: u.user,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]],
                userAvatar: '',
              });

              navigate('/');
            }
          });
      })
      .catch(console.error);
  };

  return (
    <section className="p-8">
      <h2 className="text-2xl text-center mb-4">Login</h2>
      <form className="w-1/4 mx-auto mt-12">
        <div>
          <FormInput id="email" type="email" value={ form.email } placeholder="Email Address" isRequired={ true } onChange={ updateForm('email') } />
        </div>
        <div>
          <FormInput id="password" type="password" value={ form.password } placeholder="Password" isRequired={ true } onChange={ updateForm('password') } />
        </div>
        <div className="flex justify-center">
          <Button text="Login" onClick={ login } className="w-48 bg-sky-600 text-white text-xl shadow-xl rounded-md px-4 py-2 hover:bg-orange-400 hover:text-gray-700" />
        </div>
        <div className="text-center mt-2">
          <Link to="/register" className="font-semibold text-blue-900 underline">Not a member?</Link>
        </div>
      </form>
	  </section>
  )
}
