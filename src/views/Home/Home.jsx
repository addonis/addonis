
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getFeaturedAddons, getMostDownloadedAddons, getMostRecentAddons } from "../../services/addons.services";
import { Addon } from "../../components/Addon";
import { Button } from "../../components/Button";

export const Home = () => {
  const navigate = useNavigate();

	const [featuredAddons, setFeaturedAddons] = useState([]);
	const [topAddons, setTopAddons] = useState([]);
	const [recentAddons, setRecentAddons] = useState([]);

  useEffect(() => {
    getFeaturedAddons(3).then(featured => {
      setFeaturedAddons(featured);
    }).catch(err => console.log(err));

    getMostDownloadedAddons(6).then(top => {
      setTopAddons(top);
    }).catch(err => console.log(err));

    getMostRecentAddons(6).then(recent => {
      setRecentAddons(recent);
    }).catch(err => console.log(err));
  }, []);

  return (
    <>
      <section className="p-8">
        <div className="flex justify-between items-center mb-6">
          <h2 className="text-2xl">Featured Addons</h2>
          <Button text="Show All" onClick={() => navigate('/addons/featured')} />
        </div>
        <div className="grid grid-cols-3 gap-6">
          { featuredAddons.map(data => <Addon key={ data.id } addonData={ data } />) }
        </div>
      </section>
      <section className="bg-gray-100 p-8">
        <div className="flex justify-between items-center mb-6">
          <h2 className="text-2xl">Most Downloaded Addons</h2>
          <Button text="Show All" onClick={() => navigate('/addons/top')} />
        </div>
        <div className="grid grid-cols-3 gap-6">
          { topAddons.map(data => <Addon key={ data.id } addonData={ data } />) }
        </div>
      </section>
      <section className="p-8">
        <div className="flex justify-between items-center mb-6">
          <h2 className="text-2xl">Most Recent Addons</h2>
          <Button text="Show All" onClick={() => navigate('/addons')} />
        </div>
        <div className="grid grid-cols-3 gap-6">
          { recentAddons.map(data => <Addon key={ data.id } addonData={ data } />) }
        </div>
      </section>
    </>
  );
};
