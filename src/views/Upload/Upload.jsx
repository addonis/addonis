import React from "react";
import { useState, useEffect } from "react";
import { storage } from "../../common/firebase-config";
import { ref, uploadBytes, listAll, getDownloadURL } from "firebase/storage";
import { v4 } from "uuid";

export const Upload = () => {
  const [imageUpload, setImageUpload] = useState(null);
  const [addonUpload, setAddonUpload] = useState([]);
  const addonListRef = ref(storage, "adons/");
  const uploadImage = () => {
    if (imageUpload === null) return;
    const imageRef = ref(storage, `adons/${v4()}`);
    uploadBytes(imageRef, imageUpload).then((file) => {
      alert("Upload succses");
      getDownloadURL(file.ref).then((url) => {
        setAddonUpload((prev) => [
          ...prev,
          {
            link: url,
            name: file.name,
          },
        ]);
      });
    });
  };
  useEffect(() => {
    listAll(addonListRef).then((res) => {
      res.items.forEach((item) => {
        getDownloadURL(item).then((url) => {
          setAddonUpload((prev) => [
            ...prev,
            {
              link: url,
              name: item.name,
            },
          ]);
        });
      });
    });
  }, []);

  return (
    <div>
      <div>
        <h1>Upload</h1>
        <input
          type="file"
          accept="application/x-rar-compressed"
          onChange={(e) => {
            setImageUpload(e.target.files[0]);
          }}
        />
        <button onClick={uploadImage}>Upload File</button>

        {addonUpload.map((item) => {
          return (
            <ul>
              <li>
                <a  href={item.link} key={item.name}>
                  {item.name}
                </a>
              </li>
            </ul>
          );
        })}
      </div>
    </div>
  );
};
