import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import AppContext from '../../providers/AppContext';
import { updateUser, changePassword } from '../../services/auth.service';
import { getUserByHandle, getUserData, updateUserData } from '../../services/users.services';
import { uploadFile, getUploadURL } from "../../services/uploads.services";
import { Button } from '../../components/Button';
import { FormInput } from '../../components/FormInput';
import { Upload } from "../../components/Upload";

export const Profile = () => {
  const { user, userData, userAvatar, setContext } = useContext(AppContext);
  const username = userData.username;

	const [form, setForm] = useState({
    avatar: '',
		firstName: '',
		lastName: '',
		username: '',
		email: '',
		phoneNumber: '',
		password: '',
		passwordNew: '',
		passwordConfirm: '',
  });
	
  const [avatarUpload, setAvatarUpload] = useState(null);
  const [avatarImage, setAvatarImage] = useState('');

	const [message, setMessage] = useState({
		firstName: '',
		lastName: '',
		username: '',
		email: '',
		phoneNumber: '',
		password: '',
		passwordNew: '',
		passwordConfirm: '',
	});
	
	const [status, setStatus] = useState({
		firstName: 'hidden',
		lastName: 'hidden',
		username: 'hidden',
		email: 'hidden',
		phoneNumber: 'hidden',
		password: 'hidden',
		passwordNew: 'hidden',
		passwordConfirm: 'hidden',
	});
	
	const navigate = useNavigate();
	
  useEffect(() => {
	  getUserByHandle(username)
	  .then(snapshot => {
	    if (snapshot.exists())
	    {
        const data = snapshot.val();

        setForm({
          avatar: data.avatar ?? '',
          firstName: data.firstName,
          lastName: data.lastName,
          username: data.username ? data.username : data.handle,
          email: data.email,
          phoneNumber: data.phoneNumber,
        });

        if (typeof(data.avatar) !== 'undefined' && data.avatar.length)
        {
          getUploadURL(data.avatar).then((url) => {
            setAvatarImage(url);
          });
        }

        setContext({
          user: user,
          userData: data,
          userAvatar: avatarImage,
        });
      }
    })
    .catch(console.error);
  }, [username]);

	const updateForm = prop => e => {
		setForm({
		  ...form,
		  [prop]: e.target.value,
		});
	};
	
  const changeAvatarFile = (e) => {
    setAvatarUpload(e.target.files[0]);
  };

	const profile = (e) => {
		e.preventDefault();
	
		setMessage({
		  firstName: '',
		  lastName: '',
		  username: '',
		  email: '',
      phoneNumber: '',
		  password: '',
		  passwordNew: '',
		  passwordConfirm: '',
		});
	
		setStatus({
		  firstName: 'hidden',
		  lastName: 'hidden',
		  username: 'hidden',
		  email: 'hidden',
      phoneNumber: 'hidden',
		  password: 'hidden',
		  passwordNew: 'hidden',
		  passwordConfirm: 'hidden',
		});
	
		if (form.firstName.length || form.lastName.length)
		{
      updateUser({
        displayName: `${form.firstName} ${form.lastName}`,
      });
		}
	
    /*if (form.password.length && form.passwordNew.length)
    {
      if (form.passwordNew.length < 6)
      {
        setMessage({ passwordNew: 'Password should be at least 6 characters!' });
        setStatus({ passwordNew: 'error' });
        return;
      }
    
      if (form.passwordConfirm !== form.passwordNew)
      {
        setMessage({ passwordConfirm: 'Re-typed password is different!' });
        setStatus({ passwordConfirm: 'error' });
        return;
      }

      changePassword(form.email, form.password, form.passwordNew);
    }*/
		
		// if (form.firstName.length < 6) {
		//   return alert('Password shoud be 6 or more');
		// }
		/*getUserByHandle(form.username)
		  .then(snapshot => {
			if (!snapshot.exists()) {
			  setMessage({ username: `Profile with username @${form.username} does not exists!` });
			  setStatus({ username: 'error' });
			  return;
			}*/

      const key = `avatar_${form.username.replaceAll(/[^a-zA-Z0-9\-_]+/g, '')}`;

      if (avatarUpload !== null)
      {
        uploadFile(key, avatarUpload).then((file) => {
    
          setForm({ ...form, avatar: key });
  
          getUploadURL(key).then((url) => {
            setAvatarImage(url);
          });
        })
      }
  
      const data = {
        ...form,
        avatar: (avatarUpload !== null ? key : form.avatar),
      };

			//return updateUserProfile(form.username, form.firstName, form.lastName, form.phoneNumber)
      return updateUserData(form.username, data)
				.then(() => {
          getUserData(form.username)
          .then(snapshot => {
            if (snapshot.exists()) {
              setContext({
                user: user,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]],
                userAvatar: (avatarImage ? avatarImage : userAvatar),
              });

              navigate('/profile');
            }
          });
				})
				.catch(console.error);
//			});
	  };
	
	return (
    <section className="p-8">
      <h2 className="text-2xl text-center mb-4">Update Profile</h2>
      <form className="w-1/2 mx-auto mt-12">
        <div className="grid grid-cols-3 gap-5">
          <div>
            <label htmlFor="avatar" className="block font-medium mb-2">Avatar</label>
            { avatarImage && <img src={ avatarImage } alt="" className="border border-gray-200 mb-4" /> }
            <Upload id="avatar" text="Upload Avatar Image" label="Upload Avatar Image" accept="image/png,image/jpeg,image/gif" onChange={(e) => changeAvatarFile(e)} />
          </div>
          <div className="col-span-2">
            <div>
              <FormInput id="firstName" value={ form.firstName } placeholder="First Name" label="First Name" onChange={ updateForm('firstName') } />
              <span className={status.firstName}>{message.firstName}</span>
            </div>
            <div>
              <FormInput id="lastName" value={ form.lastName } placeholder="Last Name" label="Last Name" onChange={ updateForm('lastName') } />
              <span className={status.lastName}>{message.lastName}</span>
            </div>
            <div>
              <FormInput id="username" value={ form.username } placeholder="Username" label="Username" isReadOnly={ true } onChange={ updateForm('username') } />
              <span className={status.username}>{message.username}</span>
            </div>
            <div>
              <FormInput id="email" type="email" value={ form.email } placeholder="Email Address" label="Email Address" isReadOnly={ true } onChange={ updateForm('email') } />
              <span className={status.email}>{message.email}</span>
            </div>
            <div>
              <FormInput id="phoneNumber" value={ form.phoneNumber } placeholder="Phone Number" label="Phone Number" onChange={ updateForm('phoneNumber') } />
              <span className={status.phoneNumber}>{message.phoneNumber}</span>
            </div>
            {/*<div>
              <FormInput id="password" type="password" value={ form.password } placeholder="Password" isRequired={ true } onChange={ updateForm('password') } />
              <span className={status.password}>{message.password}</span>
            </div>
            <div>
              <FormInput id="passwordConfirm" type="password" value={ form.passwordConfirm } placeholder="Re-type password" isRequired={ true } onChange={ updateForm('passwordConfirm') } />
              <span className={status.passwordConfirm}>{message.passwordConfirm}</span>
            </div>*/}
          </div>
        </div>
        <div className="flex justify-center">
          <Button text="Update" onClick={ profile } className="w-48 bg-sky-600 text-white text-xl shadow-xl rounded-md px-4 py-2 hover:bg-orange-400 hover:text-gray-700" />
        </div>
      </form>
	  </section>
  )
}
