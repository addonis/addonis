import { useState, useEffect, useContext } from 'react';
import { userRole } from '../../common/user-role';
import User from '../../components/User';
import AppContext from '../../providers/AppContext';
import { getLiveUsersData, getUsersData, updateUserRole } from '../../services/users.services';

const Users = () => {
    const { userData } = useContext(AppContext);

    const [usersData, setUsersData] = useState([]);
    const [searchField, setSearchField] = useState("");

    useEffect(() => {
        const listen = (snapshot) => {
            console.log('users changes detected');
            if (!snapshot.exists()) {
                setUsersData([]);
            }
            setUsersData(Object.keys(snapshot.val()).map(key => snapshot.val()[key]));
        }
        getUsersData().then(listen);

        const unsubscribe = getLiveUsersData(listen);

        return () => unsubscribe();
    }, []);

    const blockUser = (handle) => {
        if (handle === userData.handle) {
            alert("You can't block yourself");
            return;
        }
        updateUserRole(handle, userRole.BLOCKED);
    }
    const unblockUser = (handle) => {
        updateUserRole(handle, userRole.BASIC);
    }
    const makeAdmin = (handle) => {
        console.log("making admin" + handle);
        updateUserRole(handle, userRole.ADMIN);
    }


    const filteredUsers = usersData.filter(
        user => {
            return (
                user
                    .username
                    .toLowerCase()
                    .includes(searchField.toLowerCase()) ||
                user
                    .email
                    .toLowerCase()
                    .includes(searchField.toLowerCase())
            );
        }
    );
    const handleChange = e => {
        console.log(searchField);
        console.log(filteredUsers);
        setSearchField(e.target.value);
    };

    return (
    <section className="p-8">
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-2xl">Users</h1>
        <input
            type="search"
            placeholder="Search users"
            onChange={handleChange}
            className="border border-solid border-gray-200 px-4 py-2 outline-none"
        />
      </div>
      <div>
        <table className="w-full border-collapse">
          <thead>
            <tr>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">#</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Image</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Name</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Email</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Username</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Role</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Status</th>
              <th className="bg-gradient-to-b from-gray-100 to-gray-300 border border-solid border-gray-200 text-lg text-center text-shadow-white text-shadow-sm px-4 py-2">Actions</th>
            </tr>
          </thead>
          <tbody>
          {searchField !== "" ? (filteredUsers.map((data, index) =>
            <User key={index} index={(index + 1)} currentUserData={data} blockUser={blockUser} unblockUser={unblockUser} makeAdmin={makeAdmin} />)) :
          (usersData.map((data, index) =>
            <User key={index} index={(index + 1)} currentUserData={data} blockUser={blockUser} unblockUser={unblockUser} makeAdmin={makeAdmin} />))}
          </tbody>
        </table>
      </div>
    </section>
    );
};

export default Users;