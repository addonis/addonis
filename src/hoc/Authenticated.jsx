import { useContext } from 'react';
import { useLocation, Navigate } from 'react-router-dom';
import { FaSpinner } from 'react-icons/fa';
import AppContext from '../providers/AppContext';
import { isAuthPage, isProtectedPage } from '../common/helpers';

export default function Authenticated ({ children, loading })
{
  const { user } = useContext(AppContext);
  const location = useLocation();
  const { pathname } = location;
  const link = pathname.split('/')[1];

  if (loading)
  {
    return (
      <div className="flex absolute inset-0 justify-center items-center bg-white z-10">
        <FaSpinner className="text-6xl" />
      </div>
    );
  }

  if (!user)
  {
    if (isProtectedPage(link))
    {
      return <Navigate to="/" state={{ from: location }} />
    }
  }
  else if (isAuthPage(link))
  {
    return <Navigate to="/profile" state={{ from: location }} />
  }

  return children;
}
