import { useContext } from 'react';
import { userRole } from '../common/user-role';
import AppContext from '../providers/AppContext';
import { MenuDropdown } from './MenuDropdown';
import { MenuItem } from './MenuItem';

export const Menu = () => {
	const { user, userData} = useContext(AppContext);

	return (
		<nav className='flex'>
			<ul className='flex'>
        <MenuItem link="/" text="Home" />
        <MenuItem link="/addons" text="Addons" />
				{ user !== null && (
					<>
            <MenuItem link="/my-addons" text="My Addons" />
						{ userData.role >= userRole.ADMIN && <MenuDropdown link="/admin" text="Admin" /> }
					</>
				) }
        <MenuItem link="/about" text="About Us" />
			</ul>
		</nav>
	)
}
