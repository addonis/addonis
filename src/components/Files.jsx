import { FaRegFileAlt } from 'react-icons/fa';
import { humanFileSize } from '../common/helpers';

export const Files = ({ files }) => {
  return (
    <ul className="mb-4">
      { files.file && (
      <li className="flex justify-between items-center gap-4 border border-gray-200 px-4 py-2">
        <div>{ files.metadata.name }</div>
        <div>{ files.metadata.customMetadata?.extension }</div>
        <div>{ humanFileSize(files.size, true, 2) }</div>
        <div className="text-right">
          <a href={ files.file } target="_blank" rel="noreferrer" className="block bg-gradient-to-b from-gray-700 to-gray-800 text-white px-4 py-3 cursor-pointer"><FaRegFileAlt /></a>
        </div>
      </li>
      ) }
    </ul>
  )
}
