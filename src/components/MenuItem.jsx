import { useLocation, Link } from 'react-router-dom';

export const MenuItem = ({ id, text, link, onClick, className }) => {
  const location = useLocation();
  const { pathname } = location;

  const level = ( id ?? 1 );
  const itemLocation = link.split('/')[level];
  const currentLocation = pathname.split('/')[level];

  return (
  <li className={ level > 1 ? 'w-52 h-16' : ''}>
    <Link to={ link ?? '/' } onClick={ onClick } className={ className ?? 'flex items-center h-full border-x border-transparent ' + (itemLocation === currentLocation ? 'bg-gradient-to-b from-amber-400 to-orange-500 text-black' : 'text-white') + ' text-xl px-6 hover:bg-gradient-to-b hover:from-amber-400 hover:to-orange-500 hover:border-gray-600 hover:text-black' }>{ text }</Link>
  </li>
  )
}
