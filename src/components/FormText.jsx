
export const FormText = ({ id, value, placeholder, isRequired, isReadOnly, onChange, className, label }) => {
	return (
  <>
    { label && (<label htmlFor={ id } className="block font-medium mb-2">{ label }</label>)}
	  <textarea id={ id } value={ value } placeholder={ placeholder ?? '' } required={ isRequired ?? '' } readOnly={ isReadOnly ?? '' } onChange={ onChange } className={ className ?? 'w-full border border-solid border-gray-200 focus-visible:shadow-lg outline-none resize-none mb-4 px-4 py-2 read-only:bg-gray-100 read-only:text-gray-400 read-only:cursor-not-allowed'} />
  </>
	)
  }