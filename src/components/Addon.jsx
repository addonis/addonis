import { useEffect, useState } from "react";
import Moment from "react-moment";
import { getUserByHandle } from '../services/users.services';
import { Popups } from "../components/Popups";
import { AddonDetails } from "../views/Addon/AddonDetails";
import { Ratings } from "./Ratings";
import { Button } from "./Button";


export const Addon = ({ addonData }) => {

  const [addonAuthorData, setAddonAuthorData] = useState({});
  const [buttonPopup, setButtonPopup] = useState(false)

  useEffect(() => {
    getUserByHandle(addonData.user).then(snapshot => {
      if (snapshot.exists())
      {
        setAddonAuthorData(snapshot.val());
      }
    }).catch(e => console.log(e));


  }, []);

  return (
    <div className="flex flex-row flex-wrap w-full bg-white border border-solid border-gray-200 rounded-md shadow-lg mb-5">
      <div className="flex justify-between w-full h-20 bg-gradient-to-l from-amber-500 to-orange-500 border-b rounded-t-md p-3">
        <div>
          <h3 className="text-xl text-white text-shadow text-shadow-sm font-semibold">{ addonData.name }</h3>
          <small className="text-sm text-gray-200 italic">by { addonAuthorData.username ? addonAuthorData.username : addonAuthorData.handle }</small>
        </div>
        <div className="relative">
			    <div className="min-w-full absolute -top-7 -right-7 bg-blue-600 rounded-md text-base text-white text-center px-2 py-1">{ addonData.ide }</div>
          <Moment format="DD.MM.YYYY" className="block text-base text-gray-100 font-semibold mt-4">{ addonData.uploadDate }</Moment>
        </div>
      </div>
      <div className="flex flex-row flex-wrap justify-between w-full h-44 p-3">
        <p className="w-full text-base text-gray-700">{ (addonData.description.length > 300 ? addonData.description.substring(0, 300) + '...' : addonData.description) }</p>
        <div className="flex items-end w-full mt-4">{ addonData.tags && addonData.tags.map((tag, index) => (<span key={ index } className="inline-block bg-gray-800 rounded-xl text-xs text-white mb-1 mr-1 px-2 py-1">{ tag }</span>)) }</div>
      </div>
      <div className="flex justify-between w-full h-16 bg-gray-100 border-t rounded-b-md p-3">
        <div className="flex items-center gap-4">
          <span className="flex gap-1">
            Rating: <Ratings addon={ addonData } />
          </span>
          <span>
            Dowloads: <strong>{ addonData.downloads ?? 0 }</strong>
          </span>
        </div>
        <div>
          <Button text="details" onClick={() => setButtonPopup(true)} />
          <Popups trigger={buttonPopup} setTrigger={setButtonPopup} onOverlayClick={setButtonPopup} className="max-w-7xl" buttonClassName="text-black top-7 right-7 bg-white px-2 py-1"><AddonDetails addon={ addonData } /></Popups>
        </div>
      </div>
    </div>
  );
};
