import { useState } from "react";
import { FaStar, FaStarHalfAlt, FaRegStar } from 'react-icons/fa';
import { updateAddon } from "../services/addons.services";

export const Ratings = (props) => {

  const [addonData, setAddonData] = useState(props.addon);

  const addonRatings = (addonData.ratings ?? 0);
  const addonStars = Math.floor(addonRatings);
  const addonRemaining = parseFloat(addonRatings % addonStars);

  const stars = Array(5).fill(0);

  //console.log(addonRatings);
  //console.log(addonStars);
  //console.log(addonRemaining);
  //console.log('-------------------');

  const setRating = (value) => {

    const newRatings = parseFloat((addonData.ratings ? ((addonData.ratings + value) / 2) : value));

    updateAddon(addonData.id, { ratings: newRatings/*, [`addons/${addonData.id}/downloads`]: (addonData.downloads + 1)*/ });

    setAddonData({ ...addonData, ratings: newRatings });
  }

  return (
    <span className="flex items-center gap-0.5 text-lg text-amber-400">
      { stars.map((_, index) => {
        if (index < addonStars)
        {
          return (
          <FaStar key={ (index + 1)} onClick={() => (props.disableClick ?? setRating((index + 1)))} className={ props.disableClick ?? 'cursor-pointer' } />
          );
        }

        if (index === addonStars && !isNaN(addonRemaining) && addonRemaining > 0.0)
        {
          if (addonRemaining <= 0.5)
          {
            return (
              <FaStarHalfAlt key={ (index + 1)} onClick={() => (props.disableClick ?? setRating((index + 1)))} className={ props.disableClick ?? 'cursor-pointer' } />
            );
          }
          else
          {
            return (
              <FaStar key={ (index + 1)} onClick={() => (props.disableClick ?? setRating((index + 1)))} className={ props.disableClick ?? 'cursor-pointer' } />
            );
          }
        }
        
        return (
        <FaRegStar key={ (index + 1)} onClick={() => (props.disableClick ?? setRating((index + 1)))} className={ props.disableClick ?? 'cursor-pointer' } />
        );
      }) } { !props.hideValue ? <strong className="text-base text-black pl-1">{ addonRatings.toFixed(2) }</strong> : '' }
    </span>
  )
}
