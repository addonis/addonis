import React from 'react'
import { Button } from './Button';

export const Upload = ({ id, text, accept, onClick, onChange, className, label }) => {
  return (
	<>
    { label && <label htmlFor={ id } className="block font-medium mb-2">{ label }</label> }
    <input id={ id } type="file" accept={ accept ?? '.rar,application/vnd.rar,application/zip' } onChange={ onChange } className={ className ?? 'w-full border border-gray-200 text-md px-3 py-1 cursor-pointer focus:outline-none' } />
    { onClick && <Button text={ text } onClick={ onClick } /> }
  </>
  )
}
