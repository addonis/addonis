import { useState } from 'react';
import { useLocation, Link } from 'react-router-dom';
import { MenuItem } from "./MenuItem"

export const MenuDropdown = ({ text, link, className }) => {
  const [dropdown, setDropdown] = useState(false);

  const location = useLocation();
  const { pathname } = location;

  const itemLocation = link.split('/')[1];
  const currentLocation = pathname.split('/')[1];

  return (
    <li className="relative">
      <Link to={ link ?? '/' } aria-expanded={ dropdown ? "true" : "false" } onClick={(e) => { e.preventDefault(); setDropdown((prev) => !prev) }} className={ className ?? 'flex items-center h-full border-x border-transparent ' + (itemLocation === currentLocation ? 'bg-gradient-to-b from-amber-400 to-orange-500 text-black' : 'text-white') + ' text-xl px-6 hover:bg-gradient-to-b hover:from-amber-400 hover:to-orange-500 hover:border-gray-600 hover:text-black' }>
        { text }
      </Link>
      <ul className={ dropdown ? 'flex flex-wrap absolute top-16 bg-sky-900' : 'hidden' }>
        <MenuItem id="2" link="/admin/addons" text="Manage Addons" onClick={() => { setDropdown((prev) => !prev) }} />
        <MenuItem id="2" link="/admin/users" text="Manage Users" onClick={() => { setDropdown((prev) => !prev) }} />
      </ul>
    </li>
  )
}
