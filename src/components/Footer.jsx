import { Hidden } from "./Hidden"

export const Footer = () => {
  return (
	<footer className="w-full h-16 flex justify-between items-center fixed bottom-0 bg-sky-700 text-base text-white pl-8">
		&copy; 2022 Addonis. All Rights Reserved
		<Hidden />
	</footer>
  )
}
