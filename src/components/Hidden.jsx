import { useState, useEffect } from 'react';
import { FaSmileWink, FaSmileBeam, FaJs, FaReact, FaHeart } from 'react-icons/fa';
import { S } from '../common/helpers';
import { Button } from './Button';
import { Popups } from './Popups';

export const Hidden = () => {
  const cheet = require('cheet.js');

  const [buttonPopup, setButtonPopup] = useState(false);

  const [sM, setSM] = useState(false);
  const [sD, setSD] = useState(false);

  useEffect(() => {
    cheet(S.M[0], () => {
      setButtonPopup(false);
      setSM(true);
    });
    cheet(S.D[0], () => {
      setButtonPopup(false);
      setSD(true);
    });
  }, []);

  useEffect(() => {
    if (!buttonPopup)
    {
      setSD(false);
      setSM(false);
    }
  }, [buttonPopup]);

  return (
    <div className="flex justify-between gap-2 mr-8">
      { sM && <FaSmileWink onClick={() => setButtonPopup(true)} className="text-4xl text-white hover:text-orange-400 transition-opacity animate-pulse hover:animate-none cursor-pointer" /> }
      { sD && <FaSmileBeam onClick={() => setButtonPopup(true)} className="text-4xl text-white hover:text-orange-400 transition-opacity animate-pulse hover:animate-none cursor-pointer" /> }
      <Popups trigger={ buttonPopup } setTrigger={ setButtonPopup } className="max-w-sm">
        <div className="bg-white rounded-lg shadow-md text-black text-left z-10">
          <div className="bg-gradient-to-b from-sky-500 to-sky-800 rounded-t-lg px-4 py-3">
            <h3 className="text-lg text-white text-shadow text-shadow-sm">{ sM ? S.M[1] : S.D[1] }</h3>
          </div>
          <div className="text-center px-4 py-8">
            <h1 className="font-semibold text-3xl">Addonis</h1>
            <p className="text-lg mt-4">Build with <FaJs className="inline-block text-3xl text-yellow-400" />, <FaReact className="inline-block text-3xl text-cyan-500" /> and much <FaHeart className="inline-block text-2xl text-red-600" /> !</p>
          </div>
          <div className="flex justify-center border-t border-solid border-gray-200 text-right p-2">
            <Button text="OK" onClick={() => setButtonPopup(false)} className="w-32 bg-gradient-to-b from-amber-400 to-orange-500 border-none rounded-lg font-semibold ml-2 px-4 py-2 cursor-pointer hover:from-sky-500 hover:to-sky-800 hover:text-white" />
          </div>
        </div>
      </Popups>
    </div>
  )
}
