import { useContext } from 'react';
import AppContext from '../providers/AppContext';
import { MenuItem } from './MenuItem';
import { Logout } from './Logout';
import { Profile } from './Profile';

export const Auth = () => {
	const { user } = useContext(AppContext);

  return (
		<nav className='flex'>
			<ul className='flex'>
				{ user === null ? (
					<>
            <MenuItem link="/login" text="Login" />
            <MenuItem link="/register" text="Signup" />
					</>
				) : (
					<>
            <MenuItem link="/profile" text={<Profile />} />
						<Logout />
					</>
				) }
			</ul>
		</nav>
  )
}
