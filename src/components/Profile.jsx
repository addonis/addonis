import { useContext, useState, useEffect } from 'react';
import AppContext from '../providers/AppContext';
import { getUserByHandle, getLiveUserData } from '../services/users.services';
import { getUploadURL } from "../services/uploads.services";

export const Profile = () => {
  const { user, userData, userAvatar, setContext } = useContext(AppContext);
  const username = userData.username;

  const [profileData, setProfileData] = useState({
		name: '',
		avatar: '',
  });

  useEffect(() => {
    const listen = (snapshot) => {
      if (snapshot.exists())
      {
        const data = snapshot.val();

        if (typeof(data.avatar) !== 'undefined' && data.avatar)
        {
          getUploadURL(data.avatar).then((url) => {
            setProfileData({
              ...profileData,
              name: ((data.firstName || data.lastName) ? `${ data.firstName } ${ data.lastName }` : data.username),
              avatar: url,
            });
          });
        }
        else
        {
          setProfileData({
            ...profileData,
            name: ((data.firstName || data.lastName) ? `${ data.firstName } ${ data.lastName }` : data.username),
          });
        }
      }
    }

    getUserByHandle(username).then(listen);

    //const unsubscribe = getLiveUserData(username, listen);

    //return () => unsubscribe();
  }, []);

  return (
	<div className="flex items-center gap-4">
    { profileData.avatar && <img src={ profileData.avatar } alt="" className="w-12 h-12 rounded-full" /> }
    <div>
      <div className="text-xs text-white">Profile</div>
      { profileData.name }
    </div>
  </div>
  )
}
