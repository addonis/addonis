import { Link } from 'react-router-dom';
import { Auth } from './Auth';
import { Menu } from './Menu';

export const Header = () => {
  return (
	<header className='flex justify-between bg-gradient-to-b from-sky-600 to-sky-900 text-white'>
    <Link to="/" className='text-3xl text-shadow mx-8 my-4'>Addonis</Link>
    <Menu />
    <Auth />
  </header>
  )
}
