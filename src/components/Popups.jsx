import { Button } from "./Button";

export function Popups(props)
{
  return props.trigger ? (
    <div className="flex justify-center items-center fixed inset-0 w-full h-screen bg-black/60 z-50">
      <div className={ props.className ? `popup-inner relative w-full ${ props.className }` : 'popup-inner relative w-full max-w-2xl' }>
        { !props.hideClose ?
        <Button text="&times;" onClick={() => props.setTrigger(false)} className={ props.buttonClassName ? `absolute text-2xl ${ props.buttonClassName }` : 'absolute top-2 right-4 text-2xl text-white' } /> :
        '' }
        { props.children }
      </div>
    </div>
  ) :  ""
}
