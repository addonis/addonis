export const Button = ({ text, type, onClick, className }) => {
  return (
    <button type={ type ?? 'button' } onClick={ onClick } className={ className ?? 'bg-gradient-to-b from-gray-700 to-gray-800 text-white px-4 py-2 cursor-pointer' }>{ text }</button>
  )
}
