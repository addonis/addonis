import { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import AppContext from '../providers/AppContext';
import { logoutUser } from "../services/auth.service";
import { Button } from './Button';
import { Popups } from './Popups';

export const Logout = () => {
  const navigate = useNavigate();

	const { setContext } = useContext(AppContext);

  const [buttonPopup, setButtonPopup] = useState(false);
  
  const logout = () => {
    logoutUser()
      .then(() => {
        setContext({
          user: null,
          userData: { handle: null },
          userAvatar: ''
        });

        navigate('/');
      });
  };

  return (
  <li>
	  <Button text="&times;" onClick={() => setButtonPopup(true)} className='flex items-center h-full text-4xl text-white px-5 pb-2 hover:bg-red-600' />
    <Popups trigger={buttonPopup} setTrigger={setButtonPopup}>
      <div className="bg-white rounded-lg shadow-md z-10">
        <div className="bg-sky-800 rounded-t-lg px-4 py-3">
          <h3 className="text-lg text-white text-shadow text-shadow-sm">Logout</h3>
        </div>
        <div className="text-black text-xl px-4 py-8">Are you sure you want to logout?</div>
        <div className="flex justify-end border-t border-solid border-gray-200 text-right p-2">
          <Button text="Yes" onClick={ logout } className="bg-gradient-to-b from-green-600 to-green-900 border-none text-white px-4 py-2 cursor-pointer" />
          <Button text="No" onClick={() => setButtonPopup(false)} className="bg-gradient-to-b from-red-600 to-red-900 border-none text-white ml-2 px-4 py-2 cursor-pointer" />
        </div>
      </div>
    </Popups>
  </li>
  )
}
